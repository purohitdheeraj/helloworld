class Empty():
    x= 0

class Person():

    def set_data(self, personname, personid):
        self.name = personname
        self.id = personid

    def get_data(self):
        return ((self.name), "Hello " + str(self.id))    


obj1 = Person()
obj2 = Empty()


obj1.set_data("Dheeraj", 2)

print(obj1.get_data())

# so this way we can access the properties of class.