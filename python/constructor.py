class Job:
    # constructor
    def __init__(self, salary, location, name, qualification):
        self.name = name
        self.salary = salary
        self.location = location
        self.qualification = qualification
    
    def get_data(self):
        return self.name, self.salary, self.location, self.qualification

obj1 = Job("6.5 LPA", "Mumbai", "Dheeraj Purohit", "BE in computer engineering(2021 Batch)")

print(obj1.get_data())