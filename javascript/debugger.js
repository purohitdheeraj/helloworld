function Employee(id, name){
    this.id = id;
    this.name = name;

    this.print = function(){
        console.log("this.print", this.id, this.name);
    }

    Employee.count = 0;

    Employee.prototype.printDetails = function (){
        console.log("Employee.prototype.printDetails", this.id, this.name);
    }
}

Employee.prototype.baseSalary = 50000;

const eich = new Employee(1, "Brendan");

eich.print();
eich.printDetails();
eich.baseSalary = 1000000;

const andereesen = new Employee(2, "Marc");
// console.log(marc.baseSalary);