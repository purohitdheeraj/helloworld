# blueprint to create nodes 
class Node:
    def __init__(self, data):
        self.data = data
        self.pointer = None

# call this to add a node
class LinkedList:
    def __init__(self, data):
        self.data = data
        self.reference = None

    def addList(self, data):
        newNode = Node(data) 
        newNode.pointer = self.reference
        self.reference  = newNode

    def get_data(self):
        return self.data
 

L1 = LinkedList("Dheeraj")

# getting data
print(L1.get_data())

# adding data

L1.addList("Purohit")
print(L1.get_data())