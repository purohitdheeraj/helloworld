// operation
// add, substract, divide, multiply, modulus
// arrow functions
let add = (a,b)=>{
    return a + b
}

let substract = (a,b)=>{
    return a - b
}

let multiply = (a, b)=>{
    return a * b
}

let divide = (a, b)=>{
    return a / b
}

let modulusOperation = (a, b)=>{
    return a % b
}

console.log("Addition of : ",add(3,4));
console.log("Substraction: ",substract(8, 4));
console.log("Multiplication: ",multiply(8, 8));
console.log("Division: ",divide(10,2));
console.log("Modulus: ",modulusOperation(99, 2));