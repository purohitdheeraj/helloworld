// object literal {}
var obj1 = {
}
console.log("obj1",obj1);

// object lietral with properties 
var obj2 = {
    //  object properties
    id: 1,
    name: "Dheeraj Purohit",
    print: function(){
        console.log("obj2", this.id, this.name);
    }
};

// console.log("obj2", obj2)

obj2.print()