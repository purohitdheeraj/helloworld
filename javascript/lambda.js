// 1. simple function - definition
function sqr1(x){ return x * x}

// 2. expression function
var sqr2 = function (x){return x * x}

// 3. flat arrow function
var sqr3 = (x) =>{return x * x}

// 4. without ()
var sqr4 = x => {return x * x}

// 5.expression function 
// single parameter 
// without ()  
// arrow function
var sqr5 = x => x * x

console.log(sqr1(8));
console.log(sqr2(8));
console.log(sqr3(8));
console.log(sqr4(8));
console.log(sqr5(8));


